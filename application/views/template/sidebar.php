<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
                  <li class="header text-uppercase">Navigasi</li>
                    <!-- <li><a href='waregproject.com'><i class="fa fa-home text-primary"></i><span>Akses Website</span></a></li> -->
                    <li class=""><a href="<?php echo site_url().'admin/dashboard'?>"><i class="fa fa-dashboard"></i><span>Dashboard</span></a></li>
                    <!-- <li class="header text-uppercase">Administrasi</li> -->
                    <li class=""><a href="<?php echo site_url('warga/read') ?>"><i class="fa fa-files-o"></i><span>Data Warga</span></a></li>
                    <li class=""><a href="<?php echo site_url('admin/read') ?>"><i class="fa fa-user"></i><span>Data Admin</span></a></li>
                    <!-- <li class=""><a href="<?php echo site_url('site/bug') ?>"><i class="fa fa-comments-o"></i><span>Bugs</span></a></li> -->
                    <li class="header text-uppercase">Lain-lain</li>
                    <li class=""><a href="<?php echo site_url('admin/license') ?>"><i class="fa fa-legal"></i><span>Lisensi</span></a></li>
                </ul>
    </section>
    <!-- /.sidebar -->
</aside>

<!-- =============================================== -->

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">