</div><!-- /.content-wrapper -->

<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 2.0
    </div>
    <strong>Copyright &copy; 2017 <a href="#">Rahmad</a>.</strong> All rights reserved.
</footer>
</div><!-- ./wrapper -->

<!-- jQuery 2.1.3 -->
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/jQuery/jQuery-2.1.3.min.js') ?>"></script>
<!-- Select2 -->
<!-- <script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/select2/select2.full.min.js') ?>"></script> -->
<!-- Bootstrap 3.3.2 JS -->
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/bootstrap/js/bootstrap.min.js') ?>" type="text/javascript"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/slimScroll/jquery.slimscroll.min.js') ?>" type="text/javascript"></script>
<!-- FastClick -->
<script src='<?php echo base_url('assets/AdminLTE-2.0.5/plugins/fastclick/fastclick.min.js') ?>'></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/dist/js/app.min.js') ?>" type="text/javascript"></script>
<!-- DataTables -->
<script src='<?php echo base_url('assets/AdminLTE-2.0.5/plugins/datatables/jquery.dataTables.min.js') ?>'></script>
<script src='<?php echo base_url('assets/AdminLTE-2.0.5/plugins/datatables/dataTables.bootstrap.min.js') ?>'></script>
<!-- DataTables Reponsive-->
<script src='<?php echo base_url('assets/AdminLTE-2.0.5/plugins/datatables/extensions/Responsive/js/dataTables.responsive.js') ?>'></script>
<script src='<?php echo base_url('assets/AdminLTE-2.0.5/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js') ?>'></script>
<!-- iCheck -->
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/iCheck/icheck.min.js') ?>" type="text/javascript"></script>
<!-- InputMask -->
<script src='<?php echo base_url('assets/AdminLTE-2.0.5/plugins/input-mask/jquery.inputmask.js') ?>'></script>
<script src='<?php echo base_url('assets/AdminLTE-2.0.5/plugins/input-mask/jquery.inputmask.date.extensions.js') ?>'></script>
<script src='<?php echo base_url('assets/AdminLTE-2.0.5/plugins/input-mask/jquery.inputmask.extensions.js') ?>'></script>
<!-- date-range-picker -->
<script src='<?php echo base_url('assets/AdminLTE-2.0.5/plugins/daterangepicker/daterangepicker.js') ?>'></script>
<!-- bootstrap datepicker -->
<script src='<?php echo base_url('assets/AdminLTE-2.0.5/plugins/datepicker/bootstrap-datepicker.js') ?>'></script>
<!-- bootstrap Validator -->
<script src='<?php echo base_url('assets/js/bootstrapValidator.js') ?>'></script>
<!-- bootstrap Validator min -->
<script src='<?php echo base_url('assets/js/bootstrapValidator.min.js') ?>'></script>
