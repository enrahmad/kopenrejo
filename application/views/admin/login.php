<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Kopenrejo | RDS</title>
        <?php 
        $this->load->view('template/head');
        ?>
        <?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
    </head>
    <body class="login-page">
        <div class="login-box">
            <div class="login-box-body">
                <p class="login-box-msg">Hai, untuk menggunakan Kopenrejo RDS, silakan masukan username dan password</p>
                <form action="<?php echo base_url();?>admin/auth" method="post">
                    <?php
                        if (isset($_SESSION['psn'])) {
                            echo '<div><font color="#FF0000" >
                                    Periksa kembali Username dan password anda
                                </div>';
                            unset($_SESSION['psn']);}
                    ?>
                    <div class="form-group has-feedback">
                        <input type="text" id="username" name="username" class="form-control" placeholder="Username" required="required" />
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="password" id="password" name="password" class="form-control" placeholder="Password" required="required" />
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span> 
                    </div>
                    <div class="row">
                        <div class="col-xs-4">
                            <button type="submit" class="btn btn-primary btn-block btn-flat">Masuk</button>
                        </div><!-- /.col -->
                    </div>
                </form>
                <!-- <a href="#">Lupa Password</a><br> -->
                <br></br>
                <a href="<?php echo site_url().'admin/register'?>" class="text-center">Mendaftar admin baru</a>

            </div><!-- /.login-box-body -->
        </div><!-- /.login-box -->

        <!-- jQuery 2.1.3 -->
        <script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/jQuery/jQuery-2.1.3.min.js') ?>"></script>
        <!-- Bootstrap 3.3.2 JS -->
        <script src="<?php echo base_url('assets/AdminLTE-2.0.5/bootstrap/js/bootstrap.min.js') ?>" type="text/javascript"></script>
        
    </body>
</html>