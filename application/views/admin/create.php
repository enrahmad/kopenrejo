<?php 
$this->load->view('template/head');
?>
<!--tambahkan custom css disini-->
<?php
$this->load->view('template/topbar');
$this->load->view('template/sidebar');
?>

<?php echo form_open('admin/create'); ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Tambah Pengguna
    </h1>
</section>

<!-- Main content -->
<section class="content">
<!-- box -->
  <div class="box box-info">
    <div class="box-header with-border">
      <h3 class="box-title">Data Akun</h3>
    </div>
    <!-- form start -->
    <form class="form-horizontal">
      <div class="box-body">
        <div class="form-group">
          <label for="inputEmail3" class="col-sm-2 control-label">Nama Lengkap</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" name="nama" placeholder="">
            <span class="help-block">*required</span>
          </div>
        </div>
        <div class="form-group">
          <label for="inputEmail3" class="col-sm-2 control-label">Username</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" name="username" placeholder="">
            <span class="help-block">*required</span>
          </div>
        </div>
        <div class="form-group">
          <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
          <div class="col-sm-10">
            <input type="password" class="form-control" name="password" placeholder="">
            <span class="help-block">*required</span>
          </div>
          <label for="inputPassword4" class="col-sm-2 control-label">Password</label>
          <div class="col-sm-10">
            <input type="password" class="form-control" name="password2" placeholder="">
            <span class="help-block">*required</span>
          </div>
        </div>
      </div>
      <!-- /.box-body -->
      <div class="box-footer">
        <button type="submit" class="btn btn-default"><a href="<?php echo site_url('admin/read') ?>">Kembali</a></button>
        <button type="submit" class="btn btn-info pull-right">Tambah</button>
      </div>
      <!-- /.box-footer -->
    </form>
  </div>
</section>
<?php echo form_close(); ?>

<?php 
$this->load->view('template/js');
?>
<!--tambahkan custom js disini-->
<?php
$this->load->view('template/foot');
?>