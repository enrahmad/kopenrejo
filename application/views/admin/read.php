<?php 
$this->load->view('template/head');
?>
<!--tambahkan custom css disini-->
<?php
$this->load->view('template/topbar');
$this->load->view('template/sidebar');
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>Pengguna</h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">
                      <a href="<?php echo site_url('admin/create') ?>" class="btn btn-block btn-primary btn-flat">
                        <i class="fa fa-user-plus"></i>  Admin Baru</a></h3>
                </div>
                <div class="box-body">
                    <table id="table_user" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Nama Lengkap</th>
                                <th>Username</th>
                                <th>Pengaturan</th>
                            </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td> 
                              <?php echo 'Administrator';?></a>
                            </td>
                            <td>
                              <?php echo 'admin';?>
                            </td>
                            <td>
                              <i class="label label-warning">Aktif</i>
                            </td>
                          </tr>
                            <?php foreach($result_array as $row) {   ?>
                              <tr>
                                <td> 
                                  <?php echo $row->nama;?></a>
                                </td>
                                <td>
                                  <?php echo $row->username;?>
                                </td>
                                <!-- <td> 
                                  <?php echo $row->password;?>
                                </td> -->
                                <td>
                                  <?php echo anchor('admin/update/'.$row->id,'<i class="label label-warning">Edit</i>'); ?>
                                  <?php echo anchor('admin/delete/'.$row->id,'<i class="label label-danger">Hapus</i>'); ?>
                                </td>
                              </tr>
                            <?php }?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<?php 
$this->load->view('template/js');
?>

<!--tambahkan custom js disini-->
 <script>
   $(function () {
     $('#table_user').DataTable({
       "paging": true,
       "lengthChange": true,
       "searching": true,
       "ordering": false,
       "info": true,
       "autoWidth": false
     });
   });
 </script>

<?php
$this->load->view('template/foot');
?>