<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
?>
<?php 
$this->load->view('template/head');
?>
<!--tambahkan custom css disini-->
<?php
$this->load->view('template/topbar');
$this->load->view('template/sidebar');
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>Dashboard<small>"Be careful, you're Admin !!!"</small></h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box"><span class="info-box-icon bg-maroon"><i class="fa fa-comments-o"></i></span>
                <div class="info-box-content"><span class="info-box-text">Belum ada data</span>
                    <span class="info-box-number"><?php echo $this->db->count_all('admin');?></span></div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box"><span class="info-box-icon bg-green"><i class="fa fa-files-o"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Data warga</span>
                    <span class="info-box-number"><?php echo $this->db->count_all('warga');?></span></div>
            </div>
        </div>
        <div class="clearfix visible-sm-block"></div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box"><span class="info-box-icon bg-aqua"><i class="ion ion-ios-people-outline"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Belum ada data</span>
                    <span class="info-box-number"><?php echo $this->db->count_all('warga');?></span></div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box"><span class="info-box-icon bg-red"><i class="fa fa-user"></i></span>
                <div class="info-box-content"><span class="info-box-text">Admin</span>
                    <span class="info-box-number"><?php echo $this->db->count_all('admin');?></span></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12"></div>
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Process Manager Server</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6">
                            <p class="text-center"><strong>xxx</strong></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php 
$this->load->view('template/js');
?>
<!--tambahkan custom js disini-->
<?php
$this->load->view('template/foot');
?>