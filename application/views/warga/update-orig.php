<?php 
$this->load->view('template/head');
?>
<!--tambahkan custom css disini-->
<?php
$this->load->view('template/topbar');
$this->load->view('template/sidebar');
?>

<?php echo form_open('warga/update/'.$warga->id); ?>

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1> Update Warga </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
  </ol>
</section>
<!-- Main content -->
<section class="content">
  <div class="row">
    <!-- left column -->
    <div class="col-md-6">
      <!-- box -->
      <div class="box box-danger">
        <div class="box-header with-border">
          <h3 class="box-title">id</h3>
        </div>
        <div class="box-body">
          <div class="row">
            <div class="col-xs-12">
              <input type="text" class="form-control" name="id" value="<?php echo $warga->id ?>">
            </div>
          </div>
        </div>
        <div class="box-header with-border">
          <h3 class="box-title">Nama Lengkap</h3>
        </div>
        <div class="box-body">
          <div class="row">
            <div class="col-xs-12">
              <input type="text" class="form-control" name="nama" value="<?php echo $warga->nama ?>">
            </div>
          </div>
        </div>
        <div class="box-header with-border">
          <h3 class="box-title">Nomor Induk Kependudukan</h3>
        </div>
        <div class="box-body">
          <div class="row">
            <div class="col-xs-12">
              <input type="text" class="form-control" name="nik" value="<?php echo $warga->nik ?>">
            </div>
          </div>
        </div>
        <div class="box-header with-border">
          <h3 class="box-title">Nomor Kartu Keluarga</h3>
        </div>
        <div class="box-body">
          <div class="row">
            <div class="col-xs-12">
              <input type="text" class="form-control" name="no_kk" value="<?php echo $warga->no_kk ?>">
            </div>
          </div>
        </div>
        <div class="box-header with-border">
          <h3 class="box-title">Jenis Kelamin 
            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
            &nbsp&nbsp&nbsp&nbspAgama</h3>
        </div>
        <div class="box-body">
          <div class="row">
            <div class="col-md-6">
                <select class="form-control" style="width: 100%;" name="jk">
                  <option value="">Pilih jenis kelamin</option>
                  <?php if ($warga->jenkel == 'Laki-laki'): ?>
                    <option value="Laki-laki" selected="selected">Laki-laki</option>
                    <option value="Perempuan">Perempuan</option>
                  <?php else: ?>
                    <option value="Laki-laki">Laki-laki</option>
                    <option value="Perempuan" selected="selected">Perempuan</option>
                  <?php endif ?>
                </select>
              </div>
              <div class="col-md-6">
                <select class="form-control" style="width: 100%;" name="agama">
                  <option value="">Pilih agama</option>
                  <option value="Islam" <?= ($warga->agama == 'Islam') ? 'selected' :'' ?>>Islam</option>
                  <option value="Katolik" <?= ($warga->agama == 'Katolik') ? 'selected' :'' ?>>Katolik</option>
                  <option value="Kristen" <?= ($warga->agama == 'Kristen') ? 'selected' :'' ?>>Kristen</option>
                  <option value="Hindu" <?= ($warga->agama == 'Hindu') ? 'selected' :'' ?>>Hindu</option>
                  <option value="Budha" <?= ($warga->agama == 'Budha') ? 'selected' :'' ?>>Budha</option>
                  <option value="Konghucu" <?= ($warga->agama == 'Konghucu') ? 'selected' :'' ?>>Konghucu</option>
                </select>
              </div>
            </div>
          </div>
        <div class="box-header with-border">
          <h3 class="box-title">Tempat, tanggal lahir</h3>
        </div>
        <div class="box-body">
          <div class="row">
            <div class="col-md-6" style="width: 67%;">
              <input type="text" class="form-control" name="tempat_lahir"  value="<?= $warga->tempat_lahir ?>"> 
            </div>
          <div class="form-group">
            <div class="input-group col-md-6" style="width: 30%;">
              <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
              </div>
              <input type="text" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask name="tgl_lahir" value="<?= $warga->tgl_lahir ?>">
            </div>
          </div>
          </div>
        </div>
      </div>
    <!--/left coloumn -->
    </div>

    <!-- right column -->
    <div class="col-md-6">
      <!-- box -->
      <div class="box box-danger">
        <div class="box-header with-border">
          <h3 class="box-title">Pendidikan 
            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
            &nbsp&nbsp&nbsp&nbsp Pekerjaan</h3>
        </div>
        <div class="box-body">
          <div class="row">
            <div class="col-md-6">
              <select class="form-control" style="width: 100%;" name="pendidikan">
                <option>Pilih Pendidikan</option>
                <option <?= ($warga->pendidikan == 'SD') ? 'selected' :'' ?>>SD</option>
                <option <?= ($warga->pendidikan == 'SMP Sederajat') ? 'selected' :'' ?>>SMP Sederajat</option>
                <option <?= ($warga->pendidikan == 'SMA Sederajat') ? 'selected' :'' ?>>SMA Sederajat</option>
                <option <?= ($warga->pendidikan == 'D III') ? 'selected' :'' ?>>D III</option>
                <option <?= ($warga->pendidikan == 'S1 / D IV') ? 'selected' :'' ?>>S1 / D IV</option>
                <option <?= ($warga->pendidikan == 'S2') ? 'selected' :'' ?>>S2</option>
                <option <?= ($warga->pendidikan == 's3') ? 'selected' :'' ?>>S3</option>
              </select>
            </div>
            <div class="col-md-6">
              <select class="form-control" style="width: 100%;" name="pekerjaan">
                <option>Pilih Pekerjaan</option>
                <option <?= ($warga->pekerjaan == 'Wiraswasta') ? 'selected' :'' ?>>Wiraswasta</option>
                <option <?= ($warga->pekerjaan == 'PNS') ? 'selected' :'' ?>>PNS</option>
                <option <?= ($warga->pekerjaan == 'Buruh') ? 'selected' :'' ?>>Buruh</option>
                <option <?= ($warga->pekerjaan == 'Ibu Rumah Tangga') ? 'selected' :'' ?>>Ibu Rumah Tangga</option>
                <option <?= ($warga->pekerjaan == 'Pengajar/Guru') ? 'selected' :'' ?>>Pengajar/Guru</option>
                <option <?= ($warga->pekerjaan == 'Dosen') ? 'selected' :'' ?>>Dosen</option>
              </select>
            </div>
          </div>
        </div>    
        <div class="box-header with-border">
          <h3 class="box-title">Status Perkawinan 
            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
            &nbsp&nbsp&nbsp&nbsp Status Dalam Keluarga</h3>
        </div>
        <div class="box-body">
          <div class="row">
        <div class="col-md-6">
          <select class="form-control pekerjaan" style="width: 100%;" name="status">
            <option>Pilih status perkawinan</option>
            <option <?= ($warga->status == 'Belum Kawin') ? 'selected' :'' ?>>Belum Kawin</option>
            <option <?= ($warga->status == 'Kawin') ? 'selected' :'' ?>>Kawin</option>
            <option <?= ($warga->status == 'Janda') ? 'selected' :'' ?>>Janda</option>
            <option <?= ($warga->status == 'Duda') ? 'selected' :'' ?>>Duda</option>
          </select>
        </div>
        <div class="col-md-6">
          <select class="form-control agama" style="width: 100%;" name="status_dk">
            <option>Pilih status keluarga</option>
            <option <?= ($warga->status_dk == 'Ayah') ? 'selected' :'' ?>>Ayah</option>
            <option <?= ($warga->status_dk == 'Ibu') ? 'selected' :'' ?>>Ibu</option>
            <option <?= ($warga->status_dk == 'Anak') ? 'selected' :'' ?>>Anak</option>
            <option <?= ($warga->status_dk == 'Orang Tua') ? 'selected' :'' ?>>Orang Tua</option>
            <option <?= ($warga->status_dk == 'Mertua') ? 'selected' :'' ?>>Mertua</option>
            <option <?= ($warga->status_dk == 'Menantu') ? 'selected' :'' ?>>Menantu</option>
          </select>
        </div>
      </div>
    </div>    
        <div class="box-header with-border">
          <h3 class="box-title">Nama Ayah</h3>
        </div>
        <div class="box-body">
          <div class="row">
            <div class="col-xs-12">
              <input type="text" class="form-control" name="ayah" value="<?= $warga->ayah ?>">
            </div>
          </div>
        </div>
        <div class="box-header with-border">
          <h3 class="box-title">Nama Ibu</h3>
        </div>
        <div class="box-body">
          <div class="row">
            <div class="col-xs-12">
              <input type="text" class="form-control" name="ibu" value="<?= $warga->ibu ?>">
            </div>
          </div>
        </div>
      </div>
      <button type="submit" name="update-warga" class="btn btn-info ">Update</button>
    </div>
    <!--/right coloumn -->
  </div>
  <!-- /.row -->
</section>

<?php echo form_close(); ?>

<?php 
$this->load->view('template/js');
?>
<!--tambahkan custom js disini-->
<script type="text/javascript">
$(document).ready(function() {
  var validator = $('#formcreate').bootstrapValidator({
    feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
      nama: {
        validators: {
          notEmpty: {
            message: "Masukkan username terlebih dahulu"
          },
          stringLength: {
            min: 4,
            message: "Nama minimal 4 karakter"
          },
          remote: {
           url: '<?php echo base_url();?>warga/update_user',
           type:'POST',
           message: 'Nama sudah ada',
           data: function(validator) {
                return {
                     id: $('[name="id"]').val(),
                     nama: $('[name="nama"]').val(),
                };
           },
         }
        }
      },

      nik: {
        validators: {
          notEmpty: {
            message: "Masukkan password terlebih dahulu"
          }
        }
      },
      tgl_lahir: {
        validators: {
          notEmpty: {
            message: "Masukkan tanggal"
          }
        }
      }
      
    }
  });
});
</script>
<?php
$this->load->view('template/foot');
?>