<?php 
$this->load->view('template/head');
?>
<!--tambahkan custom css disini-->
<?php
$this->load->view('template/topbar');
$this->load->view('template/sidebar');
?>

<?php echo form_open('warga/update/'.$warga->id); ?>

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1> Edit Data Warga </h1>
</section>

<form  id="formcreate" action="<?php echo base_url();?>warga/update" method="post">
<!-- Main content -->
<section class="content">
  <div class="row">
    <!-- left column -->
    <div class="col-md-3">
      <div class="box box-warning">
        <div class="box-body">
          <h5>Nama Lengkap</h5>
          <div class="box-body">
            <div class="form-group">
              <div class="col-xs-12">
                <input type="text" class="form-control" id="nama" name="nama" value="<?php echo $warga->nama ?>">
              </div>
            </div>
          </div>
          <h5>Nomor Induk Kependudukan</h5>
          <div class="box-body">
            <div class="form-group">
              <div class="col-xs-12">
                <input type="text" class="form-control" name="nik" value="<?php echo $warga->nik ?>">
              </div>
            </div>
          </div>
          <h5>Nomor Kartu Keluarga</h5>
          <div class="box-body">
            <div class="form-group">
              <div class="col-xs-12">
                <input type="text" class="form-control" name="no_kk" value="<?php echo $warga->no_kk ?>">
              </div>
            </div>
          </div>
          <h5>Tempat Lahir</h5>
          <div class="box-body">
            <div class="form-group">
              <div class="col-md-12">
                <input type="text" class="form-control" name="tempat_lahir" value="<?php echo $warga->tempat_lahir ?>"> 
              </div>
            </div>
          </div>
          <h5>Tanggal Lahir</h5>
          <div class="box-body">
            <div class="form-group">
              <div class="col-md-12">
                <input type="text" class="form-control" placeholder="YYYY-MM-DD " name="tgl_lahir" value="<?php echo $warga->tgl_lahir ?>">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--/left coloumn -->
    <!-- %%%%%%%%%%%%%%%%%%%%% -->
    <!-- right column -->
    <div class="col-md-3">
      <div class="box box-success">
        <div class="box-body">
          <h5>Jenis Kelamin</h5>
          <div class="box-body">
            <div class="form-group">
              <div class="col-md-12">
                <select class="form-control" style="width: 100%;" name="jk" >
                  <option value="">-- Jenis Kelamin --</option>
                  <?php if ($warga->jenkel == 'Laki-laki'): ?>
                    <option value="Laki-laki" selected="selected">Laki-laki</option>
                    <option value="Perempuan">Perempuan</option>
                  <?php else: ?>
                    <option value="Laki-laki">Laki-laki</option>
                    <option value="Perempuan" selected="selected">Perempuan</option>
                  <?php endif ?>
                </select>
              </div>
            </div>
          </div>
          <h5>Agama</h5>
          <div class="box-body">
            <div class="form-group">
              <div class="col-md-12">
                <select class="form-control" style="width: 100%;" name="agama" >
                  <option value="">-- Agama --</option>
                  <option value="Islam" <?= ($warga->agama == 'Islam') ? 'selected' :'' ?>>Islam</option>
                  <option value="Katolik" <?= ($warga->agama == 'Katolik') ? 'selected' :'' ?>>Katolik</option>
                  <option value="Kristen" <?= ($warga->agama == 'Kristen') ? 'selected' :'' ?>>Kristen</option>
                  <option value="Hindu" <?= ($warga->agama == 'Hindu') ? 'selected' :'' ?>>Hindu</option>
                  <option value="Budha" <?= ($warga->agama == 'Budha') ? 'selected' :'' ?>>Budha</option>
                  <option value="Konghucu" <?= ($warga->agama == 'Konghucu') ? 'selected' :'' ?>>Konghucu</option>
                </select>
              </div>
            </div>
          </div>
          <h5>Pendidikan</h5>
          <div class="box-body">
            <div class="form-group">
              <div class="col-md-12">
                <select class="form-control" style="width: 100%;" name="pendidikan" value="<?php echo $warga->pendidikan ?>">
                  <option value="">-- Pendidikan --</option>
                  <option value="Belum Sekolah" <?= ($warga->pendidikan == 'Belum Sekolah') ? 'selected' :'' ?>>Belum Sekolah</option>
                  <option value="SD/Sederajat" <?= ($warga->pendidikan == 'SD/Sederajat') ? 'selected' :'' ?>>SD/Sederajat</option>
                  <option value="Tamat SD/Sederajat" <?= ($warga->pendidikan == 'Tamat SD/Sederajat') ? 'selected' :'' ?>>Tamat SD/Sederajat</option>
                  <option value="SLTP/Sederajat" <?= ($warga->pendidikan == 'SLTP/Sederaja') ? 'selected' :'' ?>>SLTP/Sederajat</option>
                  <option value="SLTA/Sederajat" <?= ($warga->pendidikan == 'SLTA/Sederajat') ? 'selected' :'' ?>>SLTA/Sederajat</option>
                  <option value="Mahasiswa" <?= ($warga->pendidikan == 'Mahasiswa') ? 'selected' :'' ?>>Mahasiswa</option>
                  <option value="Diploma I/II" <?= ($warga->pendidikan == 'Diploma I/II') ? 'selected' :'' ?>>Diploma I/II</option>
                  <option value="Akademi D III Sarjana" <?= ($warga->pendidikan == 'Akademi D III Sarjana') ? 'selected' :'' ?>>Akademi D III Sarjana</option>
                  <option value="Diploma IV/Strata I" <?= ($warga->pendidikan == 'Diploma IV/Strata I') ? 'selected' :'' ?>>Diploma IV/Strata I</option>
                  <option value="Strata II" <?= ($warga->pendidikan == 'Strata II') ? 'selected' :'' ?>>Strata II</option>
                </select>
              </div>
            </div>
          </div>
          <h5>Pekerjaan</h5>
          <div class="box-body">
            <div class="form-group">
              <div class="col-md-12">
                <select class="form-control" style="width: 100%;" name="pekerjaan" value="<?php echo $warga->pekerjaan ?>">
                  <option value="">-- Pekerjaan --</option>
                  <option value="Balita" <?= ($warga->pekerjaan == 'Balita') ? 'selected' :'' ?>>Balita</option>
                  <option value="Pelajar" <?= ($warga->pekerjaan == 'Pelajar') ? 'selected' :'' ?>>Pelajar</option>
                  <option value="Buruh" <?= ($warga->pekerjaan == 'Buruh') ? 'selected' :'' ?>>Buruh</option>
                  <option value="Kepolisian RI" <?= ($warga->pekerjaan == 'Kepolisian RI') ? 'selected' :'' ?>>Kepolisian RI</option>
                  <option value="Perangkat Desa" <?= ($warga->pekerjaan == 'Perangkat Desa') ? 'selected' :'' ?>>Perangkat Desa</option>
                  <option value="Pegawai Negeri Sipil" <?= ($warga->pekerjaan == 'Pegawai Negeri Sipil') ? 'selected' :'' ?>>Pegawai Negeri Sipil</option>
                  <option value="Karyawan BUMN" <?= ($warga->pekerjaan == 'Karyawan BUMN') ? 'selected' :'' ?>>Karyawan BUMN</option>
                  <option value="Karyawan Swasta" <?= ($warga->pekerjaan == 'Karyawan Swasta') ? 'selected' :'' ?>>Karyawan Swasta</option>
                  <option value="Pedagang" <?= ($warga->pekerjaan == 'Pedagang') ? 'selected' :'' ?>>Pedagang</option>
                  <option value="Wiraswasta" <?= ($warga->pekerjaan == 'Wiraswasta') ? 'selected' :'' ?>>Wiraswasta</option>
                  <option value="Petani" <?= ($warga->pekerjaan == 'Petani') ? 'selected' :'' ?>>Petani</option>
                  <option value="Ibu Rumah Tangga" <?= ($warga->pekerjaan == 'Ibu Rumah Tangga') ? 'selected' :'' ?>>Ibu Rumah Tangga</option>
                  <option value="Dosen" <?= ($warga->pekerjaan == '') ? 'selected' :'Dosen' ?>>Dosen</option>
                </select>
              </div>
            </div>
          </div>    
          <h5>Status Perkawinan</h5>
          <div class="box-body">
            <div class="form-group">
              <div class="col-md-12">
                <select class="form-control" style="width: 100%;" name="status" value="<?php echo $warga->status ?>">
                  <option value="">-- Status Perkawinan --</option>
                  <option value="Belum Kawin" <?= ($warga->status == 'Belum Kawin') ? 'selected' :'' ?>>Belum Kawin</option>
                  <option value="Kawin" <?= ($warga->status == 'Kawin') ? 'selected' :'' ?>>Kawin</option>
                  <option value="Janda" <?= ($warga->status == 'Janda') ? 'selected' :'' ?>>Janda</option>
                  <option value="Duda" <?= ($warga->status == 'Duda') ? 'selected' :'' ?>>Duda</option>
                </select>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- center coloum -->
    <!-- left coloum -->
    <div class="col-md-3">
      <div class="box box-info">
        <div class="box-body">
          <h5>Status Dalam Keluarga</h5>
          <div class="box-body">
            <div class="form-group">
              <div class="col-md-12">
                <select class="form-control" style="width: 100%;" name="status_dk" value="<?php echo $warga->status_dk ?>">
                  <option value="">-- Status Keluarga --</option>
                  <option value="Orang Tua" <?= ($warga->status_dk == 'Orang Tua') ? 'selected' :'' ?>>Orang Tua</option>
                  <option value="Kepala Rumahtangga" <?= ($warga->status_dk == 'Kepala Rumahtangga') ? 'selected' :'' ?>>Kepala Rumahtangga</option>
                  <option value="Istri" <?= ($warga->status_dk == 'Istri') ? 'selected' :'' ?>>Istri</option>
                  <option value="Anak" <?= ($warga->status_dk == 'Anak') ? 'selected' :'' ?> <?= ($warga->status_dk == '') ? 'selected' :'' ?> <?= ($warga->status_dk == '') ? 'selected' :'' ?>>Anak</option>
                  <option value="Cucu" <?= ($warga->status_dk == 'Cucu') ? 'selected' :'' ?> <?= ($warga->status_dk == '') ? 'selected' :'' ?>>Cucu</option>
                  <option value="Menantu" <?= ($warga->status_dk == 'Menantu') ? 'selected' :'' ?>>Menantu</option>
                </select>
              </div>
            </div>
          </div>
          <h5>Nama Ayah</h5>   
          <div class="box-body">
            <div class="form-group">
              <div class="col-xs-12">
                <input type="text" class="form-control" name="ayah" value="<?php echo $warga->ayah ?>">
              </div>
            </div>
          </div>
          <h5>Nama Ibu</h5>
          <div class="box-body">
            <div class="form-group">
              <div class="col-xs-12">
                <input type="text" class="form-control" name="ibu" value="<?php echo $warga->ibu ?>">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
      <!-- coloumn -->
      <!-- left coloum -->
    <div class="col-md-3">
      <div class="box box-info">
        <div class="box-body">
          <h5>Rukun Tetangga</h5>
          <div class="box-body">
            <div class="form-group">
              <div class="col-xs-12">
                <input type="text" class="form-control" name="rt" value="<?php echo $warga->rt ?>">
              </div>
            </div>
          </div>
          <h5>Rukun Warga</h5>
          <div class="box-body">
            <div class="form-group">
              <div class="col-xs-12">
                <input type="text" class="form-control" name="rw" value="<?php echo $warga->rw ?>">
              </div>
            </div>
          </div>
          <h5>Nama Gang</h5>
          <div class="box-body">
            <div class="form-group">
              <div class="col-xs-12">
                <input type="text" class="form-control" name="gang" value="<?php echo $warga->gang ?>">
              </div>
            </div>
          </div>
          <h5>Nomor Rumah</h5>
          <div class="box-body">
            <div class="form-group">
              <div class="col-xs-12">
                <input type="text" class="form-control" name="no_rumah" value="<?php echo $warga->no_rumah ?>">
              </div>
            </div>
          </div>
          <div class="box-body">
          <button type="submit" name="tambah-warga" class="btn btn-info ">Update</button>
        </div>
        </div>
      </div>
      <!-- coloumn -->
    </div>
  </div>
</section>
</form>
<?php echo form_close(); ?>
<?php 
$this->load->view('template/js');
?>
<!--tambahkan custom js disini-->
<!-- validasi form login -->

<script type="text/javascript">
$(document).ready(function() {
  var validator = $('#formcreate').bootstrapValidator({
    feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
      nama: {
        validators: {
          notEmpty: {
            message: "Field ini tidak boleh kosong"
          }
        }
      },

      nik: {
        validators: {
          notEmpty: {
            message: "Field ini tidak boleh kosong"
          },
          integer: {
            message: 'Hanya bisa diisi dengan angka'
          },
          stringLength: {
            min: 16,
            message: "NIK minimal 16 karakter"
          }
        }
      },

      no_kk: {
        validators: {
          notEmpty: {
            message: "Field ini tidak boleh kosong"
          },
          integer: {
            message: 'Hanya bisa diisi dengan angka'
          },
          stringLength: {
            min: 16,
            message: "Nomor KK minimal 16 karakter"
          }
        }
      },

      jk: {
        validators: {
          notEmpty: {
            message: "Field ini tidak boleh kosong"
          }
        }
      },

      agama: {
        validators: {
          notEmpty: {
            message: "Field ini tidak boleh kosong"
          }
        }
      },
      tempat_lahir: {
        validators: {
          notEmpty: {
            message: "Field ini tidak boleh kosong"
          }
        }
      },
      tgl_lahir: {
        validators: {
          notEmpty: {
            message: "Field ini tidak boleh kosong"
          }
        }
      },
      pendidikan: {
        validators: {
          notEmpty: {
            message: "Field ini tidak boleh kosong"
          }
        }
      },
      pekerjaan: {
        validators: {
          notEmpty: {
            message: "Field ini tidak boleh kosong"
          }
        }
      },
      status: {
        validators: {
          notEmpty: {
            message: "Field ini tidak boleh kosong"
          }
        }
      },
      status_dk: {
        validators: {
          notEmpty: {
            message: "Field ini tidak boleh kosong"
          }
        }
      },
      ayah: {
        validators: {
          notEmpty: {
            message: "Field ini tidak boleh kosong"
          }
        }
      },
      ibu: {
        validators: {
          notEmpty: {
            message: "Field ini tidak boleh kosong"
          }
        }
      },

      rt: {
        validators: {
          notEmpty: {
            message: "Field ini tidak boleh kosong"
          },
          integer: {
            message: 'Hanya bisa diisi dengan angka'
          }
        }
      },
      rw: {
        validators: {
          notEmpty: {
            message: "Field ini tidak boleh kosong"
          },
          integer: {
            message: 'Hanya bisa diisi dengan angka'
          }
        }
      },

      gang: {
        validators: {
          notEmpty: {
            message: "Field ini tidak boleh kosong"
          }
        }
      },

      no_rumah: {
        validators: {
          notEmpty: {
            message: "Field ini tidak boleh kosong"
          },
          integer: {
            message: 'Hanya bisa diisi dengan angka'
          }
        }
      }
      
    }
  });
});

</script>

<?php
$this->load->view('template/foot');
?>