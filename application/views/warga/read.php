<?php 
$this->load->view('template/head');
?>
<!--tambahkan custom css disini-->
<script>
   // $(function () {
   //   $('#table_command').table({
   //      table-layout:fixed;
   //      width:100%;
   //      overflow:hidden;
   //      word-wrap : break-word;
   //   });
   // });
 </script>


<?php
$this->load->view('template/topbar');
$this->load->view('template/sidebar');
?>

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>Data Warga Kopenrejo</h1>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"><a href="<?php echo site_url('warga/create') ?>" class="btn btn-block btn-primary btn-flat"><i class="fa fa-user-plus"></i>  Tambah Warga</a></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="table_warga" class="table table-striped table-bordered ">
            <thead>
            <tr>          
              <th><center>Nama Lengkap Warga</center></th>
              <th><center>Nomor Induk Kependudukan</center></th>
              <th><center>Nomor Kartu Keluarga</center></th>
              <th class="none">Jenis Kelamin</th>
              <th class="none">Tempat Lahir</th>
              <th class="none">Tanggal Lahir</th>
              <th class="none">Agama</th>
              <th class="none">Pendidikan</th>
              <th class="none">Pekerjaan</th>
              <th class="none">Status</th>
              <th class="none">Status Keluarga</th>
              <th class="none">Nama Ayah</th>
              <th><center>Nama Ibu</center></th>
              <th class="none">RT</th>
              <th class="none">RW</th>
              <th class="none">Gang</th>
              <th class="none">No. Rumah</th>
              <th><center>Pengaturan</center></th>
              <!-- <th>Pengaturan</th> -->
            </tr>
            <thead>
              <tbody>
            <?php foreach($result_array as $row) {   ?>
            <tr>
              <td><?php echo $row->nama;?></td>
              <td><?php echo $row->nik;?></td>
              <td><?php echo $row->no_kk;?></td>
              <td><?php echo $row->jenkel;?></td>
              <td><?php echo $row->tempat_lahir;?></td>
              <td><?php echo $row->tgl_lahir;?></td>
              <td><?php echo $row->agama;?></td>
              <td><?php echo $row->pendidikan;?></td>
              <td><?php echo $row->pekerjaan;?></td>
              <td><?php echo $row->status;?></td>
              <td><?php echo $row->status_dk;?></td>
              <td><?php echo $row->ayah;?></td>
              <td><?php echo $row->ibu;?></td>
              <td><?php echo $row->rt;?></td>
              <td><?php echo $row->rw;?></td>
              <td><?php echo $row->gang;?></td>
              <td><?php echo $row->no_rumah;?></td>
              <td><center>
                  <?php echo anchor('warga/update/'.$row->id,'<i class="label label-warning">Edit</i>'); ?>
                  <?php echo anchor('warga/delete/'.$row->id,'<i class="label label-danger">Hapus</i>'); ?>
              </center></td>
             </tr>
            <?php }?>
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>

<?php 
$this->load->view('template/js');
?>

<!--tambahkan custom js disini-->
<script>
 $(function () {
   $('#table_warga').DataTable({
      "responsive": true,
      "bAutoWidth": false,
      "aoColumns" : [
        { sWidth: '25%'}, 
        { sWidth: '22%'},
        { sWidth: '20%'},
        { sWidth: '0%'},
        { sWidth: '0%'},
        { sWidth: '0%'},
        { sWidth: '0%'},
        { sWidth: '0%'},
        { sWidth: '0%'},
        { sWidth: '0%'},
        { sWidth: '0%'},
        { sWidth: '0%'},
        { sWidth: '20%'},
        { sWidth: '0%'},
        { sWidth: '0%'},
        { sWidth: '0%'},
        { sWidth: '0%'},
        { sWidth: '15%'},
      ],
    });
 });
 




 // $(function () {
 //   $('#table_command').DataTable({
 //     "paging": true,
 //     "lengthChange": true,
 //     "searching": true,
 //     "ordering": false,
 //     "info": true,
 //     "scrollX": true,
 //     "autoWidth": true,
 //     "columnDefs": [
 //        { "width": "90%", "targets": 0 }
 //      ]
 //   });
 // });
</script>

<?php
$this->load->view('template/foot');
?>