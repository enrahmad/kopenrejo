<?php 
$this->load->view('template/head');
?>
<!--tambahkan custom css disini-->
<?php
$this->load->view('template/topbar');
$this->load->view('template/sidebar');
?>

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1> Tambah Warga </h1>
</section>

<form class="form-horizontal" id="formcreate" action="<?php echo base_url();?>warga/create" method="post">
<!-- Main content -->
<section class="content">
  <div class="row">
    <!-- left column -->
    <div class="col-md-3">
      <div class="box box-warning">
        <div class="box-body">
          <h5>Nama Lengkap</h5>
          <div class="box-body">
            <div class="form-group">
              <div class="col-xs-12">
                <input type="text" class="form-control" id="nama" name="nama" >
              </div>
            </div>
          </div>
          <h5>Nomor Induk Kependudukan</h5>
          <div class="box-body">
            <div class="form-group">
              <div class="col-xs-12">
                <input type="text" class="form-control" name="nik" >
              </div>
            </div>
          </div>
          <h5>Nomor Kartu Keluarga</h5>
          <div class="box-body">
            <div class="form-group">
              <div class="col-xs-12">
                <input type="text" class="form-control" name="no_kk" >
              </div>
            </div>
          </div>
          <h5>Tempat Lahir</h5>
          <div class="box-body">
            <div class="form-group">
              <div class="col-md-12">
                <input type="text" class="form-control" name="tempat_lahir" > 
              </div>
            </div>
          </div>
          <h5>Tanggal Lahir</h5>
          <div class="box-body">
            <div class="form-group">
              <div class="col-md-12">
                <input type="text" class="form-control" placeholder="YYYY-MM-DD " name="tgl_lahir">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--/left coloumn -->
    <!-- %%%%%%%%%%%%%%%%%%%%% -->
    <!-- right column -->
    <div class="col-md-3">
      <div class="box box-success">
        <div class="box-body">
          <h5>Jenis Kelamin</h5>
          <div class="box-body">
            <div class="form-group">
              <div class="col-md-12">
                <select class="form-control" style="width: 100%;" name="jk">
                  <option value="">-- Jenis Kelamin --</option>
                  <option value="Laki-laki">Laki-laki</option>
                  <option value="Perempuan">Perempuan</option>
                </select>
              </div>
            </div>
          </div>
          <h5>Agama</h5>
          <div class="box-body">
            <div class="form-group">
              <div class="col-md-12">
                <select class="form-control" style="width: 100%;" name="agama">
                  <option value="">-- Agama --</option>
                  <option value="Islam">Islam</option>
                  <option value="Katolik">Katolik</option>
                  <option value="Kristen">Kristen</option>
                  <option value="Hindu">Hindu</option>
                  <option value="Budha">Budha</option>
                  <option value="Konghucu">Konghucu</option>
                </select>
              </div>
            </div>
          </div>
          <h5>Pendidikan</h5>
          <div class="box-body">
            <div class="form-group">
              <div class="col-md-12">
                <select class="form-control" style="width: 100%;" name="pendidikan">
                  <option value="">-- Pendidikan --</option>
                  <option value="Belum Sekolah">Belum Sekolah</option>
                  <option value="SD/Sederajat">SD/Sederajat</option>
                  <option value="Tamat SD/Sederajat">Tamat SD/Sederajat</option>
                  <option value="SLTP/Sederajat">SLTP/Sederajat</option>
                  <option value="SLTA/Sederajat">SLTA/Sederajat</option>
                  <option value="Mahasiswa">Mahasiswa</option>
                  <option value="Diploma I/II">Diploma I/II</option>
                  <option value="Akademi D III Sarjana">Akademi D III Sarjana</option>
                  <option value="Diploma IV/Strata I">Diploma IV/Strata I</option>
                  <option value="Strata II">Strata II</option>
                </select>
              </div>
            </div>
          </div>
          <h5>Pekerjaan</h5>
          <div class="box-body">
            <div class="form-group">
              <div class="col-md-12">
                <select class="form-control" style="width: 100%;" name="pekerjaan">
                  <option value="">-- Pekerjaan --</option>
                  <option value="Balita">Balita</option>
                  <option value="Pelajar">Pelajar</option>
                  <option value="Buruh">Buruh</option>
                  <option value="Kepolisian RI">Kepolisian RI</option>
                  <option value="Perangkat Desa">Perangkat Desa</option>
                  <option value="Pegawai Negeri Sipil">Pegawai Negeri Sipil</option>
                  <option value="Karyawan BUMN">Karyawan BUMN</option>
                  <option value="Karyawan Swasta">Karyawan Swasta</option>
                  <option value="Pedagang">Pedagang</option>
                  <option value="Wiraswasta">Wiraswasta</option>
                  <option value="Petani">Petani</option>
                  <option value="Ibu Rumah Tangga">Ibu Rumah Tangga</option>
                  <option value="Dosen">Dosen</option>
                </select>
              </div>
            </div>
          </div>    
          <h5>Status Perkawinan</h5>
          <div class="box-body">
            <div class="form-group">
              <div class="col-md-12">
                <select class="form-control" style="width: 100%;" name="status">
                  <option value="">-- Status Perkawinan --</option>
                  <option value="Belum Kawin">Belum Kawin</option>
                  <option value="Kawin">Kawin</option>
                  <option value="Janda">Janda</option>
                  <option value="Duda">Duda</option>
                </select>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- center coloum -->
    <!-- left coloum -->
    <div class="col-md-3">
      <div class="box box-info">
        <div class="box-body">
          <h5>Status Dalam Keluarga</h5>
          <div class="box-body">
            <div class="form-group">
              <div class="col-md-12">
                <select class="form-control" style="width: 100%;" name="status_dk">
                  <option value="">-- Status Keluarga --</option>
                  <option value="Orang Tua">Orang Tua</option>
                  <option value="Kepala Rumahtangga">Kepala Rumahtangga</option>
                  <option value="Istri">Istri</option>
                  <option value="Anak">Anak</option>
                  <option value="Cucu">Cucu</option>
                  <option value="Menantu">Menantu</option>
                </select>
              </div>
            </div>
          </div>
          <h5>Nama Ayah</h5>   
          <div class="box-body">
            <div class="form-group">
              <div class="col-xs-12">
                <input type="text" class="form-control" name="ayah" >
              </div>
            </div>
          </div>
          <h5>Nama Ibu</h5>
          <div class="box-body">
            <div class="form-group">
              <div class="col-xs-12">
                <input type="text" class="form-control" name="ibu" >
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
      <!-- coloumn -->
      <!-- left coloum -->
    <div class="col-md-3">
      <div class="box box-info">
        <div class="box-body">
          <h5>Rukun Tetangga</h5>
          <div class="box-body">
            <div class="form-group">
              <div class="col-xs-12">
                <input type="text" class="form-control" name="rt" >
              </div>
            </div>
          </div>
          <h5>Rukun Warga</h5>
          <div class="box-body">
            <div class="form-group">
              <div class="col-xs-12">
                <input type="text" class="form-control" name="rw" >
              </div>
            </div>
          </div>
          <h5>Nama Gang</h5>
          <div class="box-body">
            <div class="form-group">
              <div class="col-xs-12">
                <input type="text" class="form-control" name="gang" >
              </div>
            </div>
          </div>
          <h5>Nomor Rumah</h5>
          <div class="box-body">
            <div class="form-group">
              <div class="col-xs-12">
                <input type="text" class="form-control" name="no_rumah" >
              </div>
            </div>
          </div>
          <div class="box-body">
          <button type="submit" name="tambah-warga" class="btn btn-info ">Tambah</button>
        </div>
        </div>
      </div>
      <!-- coloumn -->
    </div>
  </div>
</section>
</form>

<?php 
$this->load->view('template/js');
?>
<!--tambahkan custom js disini-->
<!-- validasi form login -->

<script type="text/javascript">
$(document).ready(function() {
  var validator = $('#formcreate').bootstrapValidator({
    feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
      nama: {
        validators: {
          notEmpty: {
            message: "Field ini tidak boleh kosong"
          }
        }
      },

      nik: {
        validators: {
          notEmpty: {
            message: "Field ini tidak boleh kosong"
          },
          integer: {
            message: 'Hanya bisa diisi dengan angka'
          },
          stringLength: {
            min: 16,
            message: "NIK minimal 16 karakter"
          }
        }
      },

      no_kk: {
        validators: {
          notEmpty: {
            message: "Field ini tidak boleh kosong"
          },
          integer: {
            message: 'Hanya bisa diisi dengan angka'
          },
          stringLength: {
            min: 16,
            message: "Nomor KK minimal 16 karakter"
          }
        }
      },

      jk: {
        validators: {
          notEmpty: {
            message: "Field ini tidak boleh kosong"
          }
        }
      },

      agama: {
        validators: {
          notEmpty: {
            message: "Field ini tidak boleh kosong"
          }
        }
      },
      tempat_lahir: {
        validators: {
          notEmpty: {
            message: "Field ini tidak boleh kosong"
          }
        }
      },
      tgl_lahir: {
        validators: {
          notEmpty: {
            message: "Field ini tidak boleh kosong"
          }
        }
      },
      pendidikan: {
        validators: {
          notEmpty: {
            message: "Field ini tidak boleh kosong"
          }
        }
      },
      pekerjaan: {
        validators: {
          notEmpty: {
            message: "Field ini tidak boleh kosong"
          }
        }
      },
      status: {
        validators: {
          notEmpty: {
            message: "Field ini tidak boleh kosong"
          }
        }
      },
      status_dk: {
        validators: {
          notEmpty: {
            message: "Field ini tidak boleh kosong"
          }
        }
      },
      ayah: {
        validators: {
          notEmpty: {
            message: "Field ini tidak boleh kosong"
          }
        }
      },
      ibu: {
        validators: {
          notEmpty: {
            message: "Field ini tidak boleh kosong"
          }
        }
      },

      rt: {
        validators: {
          notEmpty: {
            message: "Field ini tidak boleh kosong"
          },
          integer: {
            message: 'Hanya bisa diisi dengan angka'
          }
        }
      },
      rw: {
        validators: {
          notEmpty: {
            message: "Field ini tidak boleh kosong"
          },
          integer: {
            message: 'Hanya bisa diisi dengan angka'
          }
        }
      },

      gang: {
        validators: {
          notEmpty: {
            message: "Field ini tidak boleh kosong"
          }
        }
      },

      no_rumah: {
        validators: {
          notEmpty: {
            message: "Field ini tidak boleh kosong"
          },
          integer: {
            message: 'Hanya bisa diisi dengan angka'
          }
        }
      }
      
    }
  });
});

</script>
<?php
$this->load->view('template/foot');
?>