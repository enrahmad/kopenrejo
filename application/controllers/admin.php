<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->helper("url");
		$this->load->model('Madmin');
	}

	function index() {
		$this->load->view('admin/login');
	}
	function login() {
		$this->load->view('admin/login');
	}
	function auth() {
		$username = $this->input->post('username');
        $password = md5($this->input->post('password'));
        $par      = array($username, $password);
        $ceklog   = $this->Madmin->login($par);
        if (count($ceklog) > 0) {
            $_SESSION['admin'] = $ceklog['no'];
            redirect('admin/dashboard');
        } else {
            $_SESSION['psn'] = 'a';
            redirect('admin/login');
        }
	}
	function register() {
    $this->load->view('admin/register');
  //   $nama = $this->input->post('nama');
		// $username = $this->input->post('username');
		// $password = md5($this->input->post('password'));
		// $password2 = $this->input->post('password2');
		
		// //memasukkan variabel $nama, $use dan $desc yang telah diisi
		// //kedalam array berdasarkan nama kolom tabel
		// if (!empty($nama && $username && $password && $password2)) {
		// 	$data=array(
		// 		//nama kolom => nama variabel
		// 		'nama' => $nama,
		// 		'username' => $username,
		// 		'password' => $password,
		// 		) ;
		// 	//memasukkan array kedalam database tabel command
		// 	$this->Madmin->create($data,'admin');
		// 	$this->load->view('admin/login');
		// }
	}

	function dashboard() {
		$this->load->view('admin/dashboard');
	}
	function read() {
		$data['result_array']=$this->Madmin->read('admin')->result();
		$this->load->view('admin/read',$data);
	}
	function create() {
		$this->load->view('admin/create');
		$nama = $this->input->post('nama');
		$username = $this->input->post('username');
		$password = md5($this->input->post('password'));
		$password2 = $this->input->post('password2');
		
		//memasukkan variabel $nama, $use dan $desc yang telah diisi
		//kedalam array berdasarkan nama kolom tabel
		if (!empty($nama && $username && $password && $password2)) {
			$data=array(
				//nama kolom => nama variabel
				'nama' => $nama,
				'username' => $username,
				'password' => $password,
				) ;
			//memasukkan array kedalam database tabel command
			$this->Madmin->create($data,'admin');
		}
	}
	function license() {
		$this->load->view('admin/license');
	}
	function logout() {
		unset($_SESSION['admin']);
    redirect('admin/login');
	}
	function update() {
		$this->load->view('user/create');
	}
	function delete($id) {
		$this->Madmin->delete($id);
		redirect('admin/read');
	}

}