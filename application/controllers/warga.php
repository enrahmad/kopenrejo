<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Warga extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model('Mwarga');
	}
	function read() {
		$data['result_array']=$this->Mwarga->read('warga')->result();
		$this->load->view('warga/read',$data);	
	}
	// public function validate(){
 //    $uName = $this->input->post('nik');
 //    $isUNameCount = $this->Mwarga->isVarExists($uName);
 //    // if($isUNameCount > 0){
 //    //     return json_encode(false);
 //    // }
 //    if($isUNameCount > 0) {
 //            $isAvailable = FALSE;
 //        } else {
 //            $isAvailable = TRUE;
 //        }

 //        echo json_encode(
 //            array(
 //                'valid' => $isAvailable
 //        )); 
	// }

	public function check_user()
	{
		$nama= $this->input->post('nama'); 
		$result= $this->Mwarga->check_user_exist($nama); 
		if($result==0) {
			$isAvailable = true;
		} else {
			$isAvailable = False;
		} 
		echo json_encode(array
			( 'valid' => $isAvailable,));
	}

		public function update_user()
	{
		$id= $this->input->post('id');
		$nama= $this->input->post('nama');
		$check=$this->Mwarga->update_user_exist($nama);
		if($result==0) {
			$isAvailable = true;
		} else {
			$isAvailable = False;
		} 
		echo json_encode(array
			( 'valid' => $isAvailable,));
	}
	
    
	function create() {

		if (isset($_POST['tambah-warga'])) {

			//memasukkan data name, use dan desc kedalam variabel
			$nama 			= $this->input->post('nama');
			$nik 			= $this->input->post('nik');
			$no_kk 			= $this->input->post('no_kk');
			$jenkel 		= $this->input->post('jk');
			$tempat_lahir 	= $this->input->post('tempat_lahir');
			$tgl_lahir 		= $this->input->post('tgl_lahir');
			$agama 			= $this->input->post('agama');
			$pendidikan 	= $this->input->post('pendidikan');
			$pekerjaan 		= $this->input->post('pekerjaan');
			$status 		= $this->input->post('status');
			$status_dk 		= $this->input->post('status_dk');
			$ayah 			= $this->input->post('ayah');
			$ibu 			= $this->input->post('ibu');
			$rt 			= $this->input->post('rt');
			$rw 			= $this->input->post('rw');
			$gang 			= $this->input->post('gang');
			$no_rumah		= $this->input->post('no_rumah');


			//kedalam array berdasarkan nama kolom tabel
				$data = array(
					//nama kolom => nama variabel
					'nama' 			=> $nama,
					'nik' 			=> $nik,
					'no_kk' 		=> $no_kk,
					'jenkel' 		=> $jenkel,
					'tempat_lahir' 	=> $tempat_lahir,
					'tgl_lahir' 	=> $tgl_lahir,
					'agama' 		=> $agama,
					'pendidikan' 	=> $pendidikan,
					'pekerjaan'		=> $pekerjaan,
					'status' 		=> $status,
					'status_dk' 	=> $status_dk,
					'ayah' 			=> $ayah,
					'ibu' 			=> $ibu,
					'rt' 			=> $rt,
					'rw' 			=> $rw,
					'gang' 			=> $gang,
					'no_rumah' 		=> $no_rumah
					) ;

				//memasukkan array kedalam database tabel command
				$this->Mwarga->create($data,'warga');
				redirect('warga/read');
		} else {
			$this->load->view('warga/create');
		}
	}

	function update($id) {
		
		if (isset($_POST['update-warga'])) {
			
			//memasukkan data name, use dan desc kedalam variabel
			$nama 			= $this->input->post('nama');
			$nik 			= $this->input->post('nik');
			$no_kk 			= $this->input->post('no_kk');
			$jenkel 		= $this->input->post('jk');
			$tempat_lahir 	= $this->input->post('tempat_lahir');
			$tgl_lahir 		= $this->input->post('tgl_lahir');
			$agama 			= $this->input->post('agama');
			$pendidikan 	= $this->input->post('pendidikan');
			$pekerjaan 		= $this->input->post('pekerjaan');
			$status 		= $this->input->post('status');
			$status_dk 		= $this->input->post('status_dk');
			$ayah 			= $this->input->post('ayah');
			$ibu 			= $this->input->post('ibu');
			$rt 			= $this->input->post('rt');
			$rw 			= $this->input->post('rw');
			$gang 			= $this->input->post('gang');
			$no_rumah		= $this->input->post('no_rumah');


			//kedalam array berdasarkan nama kolom tabel
				$data = array(
					//nama kolom => nama variabel
					'nama' 			=> $nama,
					'nik' 			=> $nik,
					'no_kk' 		=> $no_kk,
					'jenkel' 		=> $jenkel,
					'tempat_lahir' 	=> $tempat_lahir,
					'tgl_lahir' 	=> $tgl_lahir,
					'agama' 		=> $agama,
					'pendidikan' 	=> $pendidikan,
					'pekerjaan'		=> $pekerjaan,
					'status' 		=> $status,
					'status_dk' 	=> $status_dk,
					'ayah' 			=> $ayah,
					'ibu' 			=> $ibu,
					'rt' 			=> $rt,
					'rw' 			=> $rw,
					'gang' 			=> $gang,
					'no_rumah' 		=> $no_rumah
					) ;

				//memasukkan array kedalam database tabel command
				$this->Mwarga->update($id, $data);
				redirect('warga/read');

		} else {
			$this->load->view('warga/update', array(
				'warga' => $this->Mwarga->getById($id)
			));
		}

		
	}
	function delete($id) {
		$this->Mwarga->delete($id);
		redirect('warga/read');
	}
}