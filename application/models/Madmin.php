<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Madmin extends CI_Model
{
	public function __construct () {
		$this->load->database();
	}

	// public function MManual(){
	// 	$query=$this->db->query("SELECT * FROM perintah");
	// 	return $query->result();
	// }
	function login ($param) {
		$sql = "SELECT * FROM `admin` WHERE `username` = ? and `password` = ?";
        $query = $this->db->query($sql, $param);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
		// $username = $this->input->POST('username', TRUE);
		// $password = md5($this->input->POST('password', TRUE));
		// $data = $this->db->query("SELECT * from admin where username='$username' AND password='$password' LIMIT 1 ");
		// return $data->row();
	}
	function read() {
		$query = $this->db->get('admin');
		return $query;
	}
	function delete ($id){
		$this->db->delete('admin',array('id'=>$id));
	}

	function create($data,$table){
		$this->db->insert($table,$data);
	}
}