<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Msite extends CI_Model
{
	public function __construct () {
		$this->load->database();
	}

	public function search($data){
		$this->db->group_by('id');
		$this->db->like('name', $data);
		$this->db->from('command');
		return $this->db->get();
	}
	// public function searchname($data){
	// 	$this->db->like('name', $data);
	// 	$this->db->from('command');
	// 	return $this->db->get();
	// }
	// public function searchuse($data){
	// 	$this->db->like('use', $data);
	// 	$this->db->from('command');
	// 	return $this->db->get();
	// }

	//mendapat parameter $keyword dari controllersite kemudian diinisial sebagai array
	public function searchArray($keyword = array()) {
		//inisial variabel $result sebagai array
		$results = array();
		//setiap isi variabel $keyword diinisalkan sebagai variabel $key, satu persatu di proses
		foreach ($keyword as $key) {
			//melemparkan $key ke func search dengan hasil berupa array $results. $key sebagai array asosiatif	
			$results[$key] = $this->search($key)->result_array();
		}
		//mengembalikan hasil berupa array $results ke controllersite
		return $results;
	}

// 	public function searchnameArray($keyword = array()) {
// 		$results = array();
// 		foreach ($keyword as $key) {
// 			$results[$key] = $this->searchname($key)->result_array();
// 		}
// 		return $results;
// 	}
// 	public function searchuseArray($keyword = array()) {
// 		$results = array();
// 		foreach ($keyword as $key) {
// 			$results[$key] = $this->searchuse($key)->result_array();
// 		}
// 		return $results;
// 	}

	// function dashboard (){
	// 	$querycommand = $this->db->count_all('command');
	// 	return $querycommand;
	// 	$this->db->count_all('user');
	// 	return $queryuser;
	// }
}