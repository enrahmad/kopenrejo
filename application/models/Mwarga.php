<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mwarga extends CI_Model{
	public function __construct () {
		$this->load->database();
	}
	function read() {
		$query = $this->db->get('warga');
		return $query;
	}
	// public function isVarExists($var){
	//     $query = $this->db
	//         ->select($var)
	//         ->where('nama', $var)
	//         ->get('warga')
	//         ->num_rows();
	//     return $query;
	// }

	public function update_user_exist($id)
	{
	    
	    $hasil=$this->db->where('id',$id);
	    $query= $this->db->get('warga');
	    if($query->num_rows()>0)
	    	{
	        	return $query->result();
	    	}
	    else
	    	{
	        	return false;
	    	}
	}

	public function check_user_exist($nama)
	{
	    $this->db->where('nama',$nama);
	    $query= $this->db->get('warga');
	    if($query->num_rows()>0)
	    	{
	        	return $query->result();
	    	}
	    else
	    	{
	        	return false;
	    	}
	}

	function create($data,$table){
		$this->db->insert($table,$data);
	}
	function delete ($id){
		$this->db->delete('warga',array('id'=>$id));
	}

	public function getById($id) {
		$this->db->where('id', $id);
		$query = $this->db->get('warga');
		return $query->result()[0];
	}

	public function update($id, $data) {
		$this->db->where('id', $id);
    	$this->db->update('warga', $data);
	}
}
