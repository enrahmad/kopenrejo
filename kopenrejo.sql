/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 100119
Source Host           : localhost:3306
Source Database       : kopenrejo

Target Server Type    : MYSQL
Target Server Version : 100119
File Encoding         : 65001

Date: 2017-02-17 22:29:34
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES ('1', 'admin kopenrejo', 'admin', '21232f297a57a5a743894a0e4a801fc3');
INSERT INTO `admin` VALUES ('3', 'we', 'we', 'ff1ccf57e98c817df1efcd9fe44a8aeb');

-- ----------------------------
-- Table structure for warga
-- ----------------------------
DROP TABLE IF EXISTS `warga`;
CREATE TABLE `warga` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `nik` int(30) NOT NULL,
  `no_kk` int(30) NOT NULL,
  `jenkel` varchar(255) NOT NULL,
  `tempat_lahir` varchar(50) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `agama` varchar(255) NOT NULL DEFAULT 'ISLAM',
  `pendidikan` varchar(255) NOT NULL,
  `pekerjaan` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `status_dk` varchar(255) NOT NULL,
  `ayah` varchar(255) NOT NULL,
  `ibu` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of warga
-- ----------------------------
INSERT INTO `warga` VALUES ('1', 'navikula dilog dini hari sedang beraksi', '123', '456', 'W', 'jogja', '2017-01-30', 'islam', 's1', 'buruh', 'janda', 'anak', 'paijem', 'paijo');
INSERT INTO `warga` VALUES ('2', 'a', '0', '0', 'a', 'a', '0000-00-00', 'a', 'a', '', 'a', 'a', 'a', 'a');
SET FOREIGN_KEY_CHECKS=1;
